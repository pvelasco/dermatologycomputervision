#include "myqgraphicsview.h"
#include <QFile>
#include <QCoreApplication>
#include <QTextStream>
#include <iostream>

MyQGraphicsView::MyQGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
    scene = new QGraphicsScene();
    //this->setSceneRect(50, 50, 350, 350);
    this->setScene(scene);
}

void MyQGraphicsView::mousePressEvent(QMouseEvent * e)
{
	if(e->button() == Qt::LeftButton)
    {
		QPointF pt = mapToScene(e->pos());

        QString line = QString::number(pt.x()) + " " +  QString::number(pt.y());

		QFile file("datos.txt");
		file.open(QIODevice::WriteOnly | QIODevice::Append);
		QTextStream out(&file);
		out << line << endl;
		file.close();
    }
}