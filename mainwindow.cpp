#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QFile>
#include <QCoreApplication>
#include <QTextStream>

#include <openCVToQt.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo//photo.hpp>

using namespace cv;

QStringList points;
Mat input_image;
Mat image_modified;
int blur_type = 0;

QImage Mat2QImage(Mat const& src)
{
     Mat temp(src.cols,src.rows,src.type()); // make the same Mat
     cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copt, that what i need
     QImage dest= QImage((uchar*) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
     return dest;
}

Mat QImage2Mat(QImage const& src)
{
     QImage temp = src.copy();
     Mat res(temp.height(),temp.width(),CV_8UC3,(uchar*)temp.bits(),temp.bytesPerLine());
     cvtColor(res, res,CV_BGR2RGB); // make convert colort to BGR !
     return res;
}

void read_file()
{
	QFile file("datos.txt");
    QTextStream in(&file);
	file.open(QIODevice::ReadOnly | QIODevice::Text);

	while (!in.atEnd()) {
		QString line = in.readLine();

		points.append(line.split(" "));
    }
}

void Erosion(Mat src, Mat erosion_dst)
{
  int erosion_type = MORPH_ELLIPSE ; // MORPH_RECT MORPH_CROSS  MORPH_ELLIPSE
  int erosion_size = 10; //va de 0 a 21

  Mat element = getStructuringElement( erosion_type, Size( 2*erosion_size + 1, 2*erosion_size+1 ), Point( erosion_size, erosion_size ) );
  
  /// Apply the erosion operation
  erode( src, erosion_dst, element );
}

void Dilation(Mat src, Mat dilation_dst)
{
  int dilation_type = MORPH_RECT; // MORPH_RECT MORPH_CROSS  MORPH_ELLIPSE
  int dilation_size = 17; //va de 0 a 21

  Mat element = getStructuringElement( dilation_type, Size( 2*dilation_size + 1, 2*dilation_size+1 ), Point( dilation_size, dilation_size ) );
  
  /// Apply the dilation operation
  dilate( src, dilation_dst, element );
}

void MainWindow::drawQTImage(QImage image)
{
	//QGraphicsScene puede contener muchos elementos 2D de QT
    QGraphicsScene* scene = new QGraphicsScene();

    //El graphicsView es el widget "el contenedor" del graphicsScene
    ui->graphicsView->setScene(scene);

    //muestra una imagen en un graphicsScene
    QGraphicsPixmapItem* item;

    //muestra una imagen en un graphicsScene
    /*if(image.size().width() > 641 || image.size().height() > 445)
        item = new QGraphicsPixmapItem(QPixmap::fromImage(image).scaled(QSize(641,445),  Qt::KeepAspectRatio));
    else*/
    item = new QGraphicsPixmapItem(QPixmap::fromImage(image));

    scene->addItem(item);

    ui->graphicsView->show();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	remove("datos.txt");

    ui->setupUi(this);


    ui->label->setVisible(false);
    ui->label_2->setVisible(false);
    ui->label_3->setVisible(false);
    ui->label_4->setVisible(false);

    ui->textEdit->setVisible(false);
    ui->textEdit_2->setVisible(false);
    ui->textEdit_3->setVisible(false);
    ui->textEdit_4->setVisible(false);

    ui->applyButton->setVisible(false);

    connect(ui->actionLoad_Image, SIGNAL(triggered()), this, SLOT(open()));
    connect(ui->actionBlur, SIGNAL(triggered()), this, SLOT(blur_image()));
    connect(ui->bt_reset, SIGNAL(clicked()), this, SLOT(bt_reset_onclick()));
    connect(ui->actionBox_Filtering, SIGNAL(triggered()), this, SLOT(boxFilter_image()));
    connect(ui->actionBilateral_Filter , SIGNAL(triggered()), this, SLOT(bilateralFilter_image()));
    connect(ui->actionGaussian_Blur, SIGNAL(triggered()), this, SLOT(GaussianBlur_image()));
    connect(ui->actionMedian_Blur, SIGNAL(triggered()), this, SLOT(medianBlur_image()));
    connect(ui->applyButton, SIGNAL(clicked()),this, SLOT(bt_apply_click()));
	connect(ui->showButton, SIGNAL(clicked()),this, SLOT(bt_show_click()));
	connect(ui->actionColor_Histogram, SIGNAL(triggered()), this, SLOT(ColorHistogram()));
	connect(ui->findContoursButton, SIGNAL(clicked()),this, SLOT(findContoursClick()));
	connect(ui->actionRGB, SIGNAL(triggered()), this, SLOT(convert_to_RGB()));
	connect(ui->actionCanny, SIGNAL(triggered()), this, SLOT(cannyFilter()));
	connect(ui->actionSobel, SIGNAL(triggered()), this, SLOT(sobelFilter()));
	connect(ui->actionLaplace, SIGNAL(triggered()), this, SLOT(laplaceFilter())); 
	connect(ui->actionABCD, SIGNAL(triggered()), this, SLOT(ABCD_method())); 
}

MainWindow::~MainWindow()
{
	
    delete ui;
}


void MainWindow::open()
{
     QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"), QDir::currentPath());
     if (!fileName.isEmpty()) {
		 input_image = imread( fileName.toStdString(), CV_LOAD_IMAGE_COLOR );
		 input_image.copyTo(image_modified);

		 QImage image = mat_to_qimage_cpy(input_image,true);
         drawQTImage(image);
     }
}

void MainWindow::bt_apply_click()
{
    Mat cvImage_src, cvImage_dst;
	input_image.copyTo(cvImage_src);

    switch(blur_type)
    {
        case 1:
            blur( cvImage_src, cvImage_dst, Size(ui->textEdit->toPlainText().toInt(),ui->textEdit_2->toPlainText().toInt()));
            break;
        case 2:
            boxFilter( cvImage_src, cvImage_dst, ui->textEdit_3->toPlainText().toInt(), Size(ui->textEdit->toPlainText().toInt(),ui->textEdit_2->toPlainText().toInt()));
            break;
        case 3:
			bilateralFilter( cvImage_src, cvImage_dst, 30, 100, 100);
            break;
        case 4:
            GaussianBlur( cvImage_src, cvImage_dst, Size(ui->textEdit->toPlainText().toInt(),ui->textEdit_2->toPlainText().toInt()), ui->textEdit_3->toPlainText().toInt(), ui->textEdit_4->toPlainText().toInt());
            break;
        case 5:
            medianBlur( cvImage_src, cvImage_dst, ui->textEdit->toPlainText().toInt());
            break;
    }

	image_modified = cvImage_dst;
    QImage image = mat_to_qimage_cpy(cvImage_dst,true);

    drawQTImage(image);
}

void MainWindow::bt_show_click()
{
	Mat item;

	Size s( 641, 445 );
	cv::resize( image_modified, item, s, 0, 0, CV_INTER_AREA );

	namedWindow("show button click", CV_WINDOW_AUTOSIZE );
	imshow("show button click", item);

	cvWaitKey(0);
}

void MainWindow::blur_image()
{

    ui->label->setText("Size rows: ");
    ui->label->setVisible(true);
    ui->label_2->setText("Size cols: ");
    ui->label_2->setVisible(true);

    ui->textEdit->setText("15");
    ui->textEdit->setVisible(true);
    ui->textEdit_2->setText("15");
    ui->textEdit_2->setVisible(true);
    ui->applyButton->setVisible(true);

    ui->label_4->setVisible(false);
    ui->textEdit_4->setVisible(false);
    ui->label_3->setVisible(false);
    ui->textEdit_3->setVisible(false);

    blur_type = 1;
}

void MainWindow::boxFilter_image()
{

    ui->label->setText("Size rows: ");
    ui->label->setVisible(true);
    ui->label_2->setText("Size cols: ");
    ui->label_2->setVisible(true);
    ui->label_3->setText("Depth: ");
    ui->label_3->setVisible(true);

    ui->textEdit->setText("9");
    ui->textEdit->setVisible(true);
    ui->textEdit_2->setText("9");
    ui->textEdit_2->setVisible(true);
    ui->textEdit_3->setText("-1");
    ui->textEdit_3->setVisible(true);

    ui->applyButton->setVisible(true);

    ui->label_4->setVisible(false);
    ui->textEdit_4->setVisible(false);

    blur_type = 2;
}

void MainWindow::bilateralFilter_image()
{
	ui->applyButton->setVisible(true);

	 blur_type = 3;
}

void MainWindow::GaussianBlur_image()
{
    ui->label->setText("Size rows: ");
    ui->label->setVisible(true);
    ui->label_2->setText("Size cols: ");
    ui->label_2->setVisible(true);
    ui->label_3->setText("Sigma X: ");
    ui->label_3->setVisible(true);
    ui->label_4->setText("Sigma Y: ");
    ui->label_4->setVisible(true);

    ui->textEdit->setText("9");
    ui->textEdit->setVisible(true);
    ui->textEdit_2->setText("9");
    ui->textEdit_2->setVisible(true);
    ui->textEdit_3->setText("1");
    ui->textEdit_3->setVisible(true);
    ui->textEdit_4->setText("0");
    ui->textEdit_4->setVisible(true);

    ui->applyButton->setVisible(true);

    blur_type = 4;
}

void MainWindow::medianBlur_image()
{
    ui->label->setText("Ksize: ");
    ui->label->setVisible(true);

    ui->textEdit->setText("9");
    ui->textEdit->setVisible(true);

    ui->applyButton->setVisible(true);

    ui->label_2->setVisible(false);
    ui->textEdit_2->setVisible(false);
    ui->label_3->setVisible(false);
    ui->textEdit_3->setVisible(false);
    ui->label_4->setVisible(false);
    ui->textEdit_4->setVisible(false);

    blur_type = 5;
}

void MainWindow::bt_reset_onclick()
{
    //QGraphicsScene puede contener muchos elementos 2D de QT
    QGraphicsScene* scene = new QGraphicsScene();
	input_image.copyTo(image_modified);

    //El graphicsView es el widget "el contenedor" del graphicsScene
    ui->graphicsView->setScene(scene);

    //muestra una imagen en un graphicsScene
    QGraphicsPixmapItem* item;
	item = new QGraphicsPixmapItem(QPixmap::fromImage(mat_to_qimage_cpy(input_image,true)));

    scene->addItem(item);

    ui->graphicsView->show();

	ui->label->setVisible(false);
    ui->label_2->setVisible(false);
    ui->label_3->setVisible(false);
    ui->textEdit->setVisible(false);
    ui->textEdit_2->setVisible(false);
    ui->textEdit_3->setVisible(false);

    ui->applyButton->setVisible(false);
}

void MainWindow::ColorHistogram()
{
    //setimageROI <- cropping
	Mat src; Mat hsv; Mat hue;
	int bins = 25;
    src = image_modified;
	
	cvtColor(src,hsv, CV_BGR2HSV);

	MatND hist;
    int histSize[] = {30, 32};
	float hranges[] = { 0, 180 };
	float sranges[] = { 0, 256 };

	const float* ranges[] = { hranges, sranges };
	
	int miau[] = {0, 1};

	 /// Get the Histogram and normalize it
	calcHist( &hsv, 1, miau, Mat(), hist, 2, histSize, ranges, true, false );
	normalize( hist, hist, 0, 255, NORM_MINMAX, -1, Mat() );

	open();

	src = image_modified;
	
	cvtColor(src,hsv, CV_BGR2HSV);
	
	blur_image();
	bt_apply_click();

	/// Get Backprojection
	MatND backproj;
	calcBackProject( &hsv, 1, miau, hist, backproj, ranges, 1, true );

	backproj.copyTo(image_modified);

	QImage image = mat_to_qimage_cpy(backproj,true);

    drawQTImage(image);
}


void ColorHistogramAUXX()
{
    //setimageROI <- cropping
    Mat src, dst;

  /// Load image
  image_modified.copyTo(src);

  /// Separate the image in 3 places ( B, G and R )
  vector<Mat> bgr_planes;
  split( src, bgr_planes );

  /// Establish the number of bins
  int histSize = 256;

  /// Set the ranges ( for B,G,R) )
  float range[] = { 0, 256 } ;
  const float* histRange = { range };

  bool uniform = true; bool accumulate = false;

  Mat b_hist, g_hist, r_hist;

  /// Compute the histograms:
  calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );

  // Draw the histograms for B, G and R
  int hist_w = 512; int hist_h = 400;
  int bin_w = cvRound( (double) hist_w/histSize );

  Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );

  /// Normalize the result to [ 0, histImage.rows ]
  normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
  normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
  normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

  /// Draw for each channel
  for( int i = 1; i < histSize; i++ )
  {
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
                       Scalar( 255, 0, 0), 2, 8, 0  );
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
                       Scalar( 0, 255, 0), 2, 8, 0  );
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
                       Scalar( 0, 0, 255), 2, 8, 0  );
  }

  /// Display
  namedWindow("calcHist Demo", CV_WINDOW_AUTOSIZE );
  imshow("calcHist Demo", histImage );

  cvWaitKey(0);

  int threshold = histImage.rows*0.2;
  int distance = 1000;

  /*for(int i=0;i<256;i++)
  {
	  if((b_hist.at<int>(i)-threshold) < distance)
  }*/
}

void contarBlancos(Mat image, int x, int y)
{
	int count = 0;

	int k = x-2; //son las rows
	int w = y-2; //son las cols
	
	if(x < 2) 
		k = 0;

	if(x+2 > image.rows-1)
		k = image.rows-5;

	if(w < 2) 
		w = 0;

	if(y+2 > image.cols-1)
		w = image.cols-5;
	

	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			if(image.at<uchar>(k+j,w+i) == 255)
				count++;
		}
	}
	
	if(count < 5)
	{
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
			{
				image.at<uchar>(k+j,w+i) = 0;
			}
		}
	}

}

void applyFilter(Mat image)
{
	int kTam = 5;
	Mat aux;

	for(int i=0; i < image.cols ; i++)
	{
		for(int j=0; j < image.rows ; j++)
		{
			//El m�todo AT recibe como parametros (rows,cols). Donde en una matriz NxM N => cols y M => rows. 
			if(image.at<uchar>(j,i) == 255)
				contarBlancos(image,j,i); 

			//crea una submatriz de punteros (si quisieramos literal, tenemos que hacer M.col(7).copyTo(M1))
			//aux = image.colRange(i,i+5).rowRange(j,j+5);

			/*if(contarBlancos(aux) < 8) //menos 30% de blancos.... borralo
			{

			}*/
		}
	}
}

void MainWindow::CannyThreshold(int value)
{
	/*Mat cvImage_src, cvImage_dst, cvImage_gray;
    cvImage_src = image_modified;

	//adaptiveThreshold(cvImage_src,cvImage_dst,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,11,2);
	threshold(cvImage_src,cvImage_dst,-1,255, THRESH_BINARY_INV+THRESH_OTSU);

	QImage image = mat_to_qimage_cpy(cvImage_dst,true);
	
	drawQTImage(image);*/
}

void MainWindow::laplaceFilter()
{
	Mat cvImage_src, cvImage_dst, cvImage_gray;
	cvImage_src = image_modified;

	/// Convert the image to grayscale
	cvtColor( cvImage_src, cvImage_gray, CV_BGR2GRAY );

	
	/// Apply Laplace function
	int kernel_size = 3;
	int scale = 1;
	int delta = 0;

	Laplacian( cvImage_gray, cvImage_dst, CV_16S, kernel_size, scale, delta, BORDER_DEFAULT );
	convertScaleAbs( cvImage_dst, image_modified );

	QImage image = mat_to_qimage_cpy(image_modified,true);

    drawQTImage(image);
}

void MainWindow::sobelFilter()
{
	Mat cvImage_src, cvImage_dst, cvImage_gray;
	cvImage_src = image_modified;

	cvtColor(cvImage_src,cvImage_gray, CV_BGR2GRAY);

	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;
	Mat grad;
	
	//Scharr(cvImage_gray, grad_x, CV_16S, 1, 0, 3, 1, BORDER_DEFAULT );
	Sobel(cvImage_gray, grad_x, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT );
	convertScaleAbs( grad_x, abs_grad_x );

	/// Gradient Y
	//Scharr( cvImage_gray, grad_y, CV_16S, 0, 1, 3, 1, BORDER_DEFAULT );
	Sobel( cvImage_gray, grad_y, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT );
	convertScaleAbs( grad_y, abs_grad_y );

	/// Total Gradient (approximate)
	addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

	//Dilation(grad,grad);

	grad.copyTo(image_modified);

	QImage image = mat_to_qimage_cpy(image_modified,true);

    drawQTImage(image);
}

void MainWindow::cannyFilter()
{
	Mat cvImage_src, cvImage_dst, cvImage_gray;
    cvImage_src = image_modified;

	cvtColor( cvImage_src, cvImage_gray, CV_BGR2GRAY );

	int lowThreshold = 6; //entre 0 y 100
	int ratio = 3; //recomiendan dejarlo fijo
	int kernel_size = 3; //3,5,7 (el mejor es 3)

	/// Detect edges using canny
	Canny(cvImage_gray, image_modified, lowThreshold, lowThreshold*ratio, kernel_size);

	
	Dilation(image_modified,image_modified);
	Erosion(image_modified, image_modified);

	QImage image = mat_to_qimage_cpy(image_modified,true);

    drawQTImage(image);
}

void MainWindow::findContoursClick()
{
	Mat cvImage_src, cvImage_dst, cvImage_gray;
    cvImage_src = image_modified;

	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	/// Find contours
	findContours( cvImage_src, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	/// Draw contours
	Mat drawing = Mat::zeros( cvImage_src.size(), CV_8UC3 );
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());

	RNG rng;

	for( int i = 0; i< contours.size(); i++ )
	{
		Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		minEnclosingCircle((Mat)contours[i],center[i],radius[i]);

		circle(drawing,center[i], (int)radius[i],color,2,8,0);
		drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
	}

	drawing.copyTo(image_modified);

	/// Show in a window
	QImage image = mat_to_qimage_cpy(image_modified,true);

    drawQTImage(image);
}

void MainWindow::convert_to_RGB()
{
	read_file();

	Mat cvImage_src, aux, mask1, mask2, cvImage_output;
    aux = image_modified;
	
	cvtColor(aux,cvImage_src, CV_BGR2HSV);

	std::vector<Mat> HSV;
    split(cvImage_src, HSV);

	int cont = 0;
	int media1[] = { 0, 0, 0};
	int min1[] = {255, 255, 255};
	int max1[] = {0, 0, 0};


	int media2[] = { 0, 0, 0};
	int min2[] = {255, 255, 255};
	int max2[] = {0, 0, 0};

	//Primer area de c�lculo
	for(int i=points[0].toInt()-24;i< points[0].toInt()+25;i++)
	{
		for(int j=points[1].toInt()-24;j<points[1].toInt()+25;j++)
		{
			HSV[0].at<uchar>(j,i);
			media1[1] += (int) HSV[1].at<uchar>(j,i);
			media1[2] += (int) HSV[2].at<uchar>(j,i);

			if(HSV[0].at<uchar>(j,i) < min1[0]) min1[0] = HSV[0].at<uchar>(j,i);
			if(HSV[1].at<uchar>(j,i) < min1[1]) min1[1] = HSV[1].at<uchar>(j,i);
			if(HSV[2].at<uchar>(j,i) < min1[2]) min1[2] = HSV[2].at<uchar>(j,i);
			
			if(HSV[0].at<uchar>(j,i) > max1[0]) max1[0] = HSV[0].at<uchar>(j,i);
			if(HSV[1].at<uchar>(j,i) > max1[1]) max1[1] = HSV[1].at<uchar>(j,i);
			if(HSV[2].at<uchar>(j,i) > max1[2]) max1[2] = HSV[2].at<uchar>(j,i);
			
			cont++;
		}
	}

	media1[0] /= cont;
	media1[1] /= cont;
	media1[2] /= cont;

	std::vector<int> tres1;
	std::vector<int> tres2;

	tres1.push_back(media1[0] - (abs(min1[0]-max1[0])/2));
	tres1.push_back(media1[1] - (abs(min1[1]-max1[1])/2));
	tres1.push_back(media1[2] - (abs(min1[2]-max1[2])/2));


	tres2.push_back(media1[0] + (abs(min1[0]-max1[0])/2));
	tres2.push_back(media1[1] + (abs(min1[1]-max1[1])/2));
	tres2.push_back(media1[2] + (abs(min1[2]-max1[2])/2));
	
	cont = 0;

	inRange(cvImage_src,tres1,tres2,mask1);


	//Segundo area de c�lculo
	for(int i=points[2].toInt()-9;i<points[2].toInt()+10;i++)
	{
		for(int j=points[3].toInt()-9;j<points[3].toInt()+10;j++)
		{

			media2[0] += (int) HSV[0].at<uchar>(j,i);
			media2[1] += (int) HSV[1].at<uchar>(j,i);
			media2[2] += (int) HSV[2].at<uchar>(j,i);

			if(HSV[0].at<uchar>(j,i) < min2[0]) min2[0] = HSV[0].at<uchar>(j,i);
			if(HSV[1].at<uchar>(j,i) < min2[1]) min2[1] = HSV[1].at<uchar>(j,i);
			if(HSV[2].at<uchar>(j,i) < min2[2]) min2[2] = HSV[2].at<uchar>(j,i);
			
			if(HSV[0].at<uchar>(j,i) > max2[0]) max2[0] = HSV[0].at<uchar>(j,i);
			if(HSV[1].at<uchar>(j,i) > max2[1]) max2[1] = HSV[1].at<uchar>(j,i);
			if(HSV[2].at<uchar>(j,i) > max2[2]) max2[2] = HSV[2].at<uchar>(j,i);
			
			cont++;
		}
	}

	media2[0] /= cont;
	media2[1] /= cont;
	media2[2] /= cont;

	tres1.clear();
	tres2.clear();

	tres1.push_back(media2[0] - (abs(min2[0]-max2[0])/2));
	tres1.push_back(media2[1] - (abs(min2[1]-max2[1])/2));
	tres1.push_back(media2[2] - (abs(min2[2]-max2[2])/2));


	tres2.push_back(media2[0] + (abs(min2[0]-max2[0])/2));
	tres2.push_back(media2[1] + (abs(min2[1]-max2[1])/2));
	tres2.push_back(media2[2] + (abs(min2[2]-max2[2])/2));

	inRange(cvImage_src,tres1,tres2,mask2);

	cvImage_output = mask1 + mask2;

	Mat item, itme2;

	
	Dilation(cvImage_output, cvImage_output);
	Erosion(cvImage_output, cvImage_output);
	bitwise_and(image_modified,image_modified, item, cvImage_output);

	Size s( 641, 445 );
	cv::resize(item , itme2, s, 0, 0, CV_INTER_AREA );

	namedWindow("dibujar caja", CV_WINDOW_AUTOSIZE );
	imshow("dibujar caja", itme2);

	cvWaitKey(0);
	
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	/// Find contours
	findContours( cvImage_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	/// Draw contours
	Mat drawing = Mat::zeros( cvImage_output.size(), CV_8UC3 );
	RNG rng;

	int maxContours = 0;
	int idxContour = 0;

	for( int i = 0; i< contours.size(); i++ )
	{
		if(contours[i].size() > maxContours)
		{
			maxContours = contours[i].size();
			idxContour = i;
		}
	}


	//dibujar la caja
	Rect r1 = boundingRect( Mat(contours[idxContour]) );

	rectangle(input_image,r1.tl(),r1.br(), Scalar(255,255,255));


	Mat i3;
	cv::resize( input_image, i3, s, 0, 0, CV_INTER_AREA );

	namedWindow("dibujar caja2", CV_WINDOW_AUTOSIZE );
	imshow("dibujar caja2", i3);

	cvWaitKey(0);
}

void MainWindow::ABCD_method()
{
	
	RNG rng;
	Mat src = image_modified;
	Mat src_gray;
	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	cvtColor( src, src_gray, CV_BGR2GRAY );
	findContours( src_gray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

  /// Get the moments
  vector<Moments> mu(contours.size() );
  for( int i = 0; i < contours.size(); i++ )
     { mu[i] = moments( contours[i], false ); }

  ///  Get the mass centers:
  vector<Point2f> mc( contours.size() );
  for( int i = 0; i < contours.size(); i++ )
     { mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }

  /// Draw contours
  Mat drawing = Mat::zeros( src.size(), CV_8UC3 );
  for( int i = 0; i< contours.size(); i++ )
	{
		Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
	}

  /// Show in a window
  namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
  imshow( "Contours", drawing );

  int area;
	int p;

  /// Calculate the area with the moments 00 and compare with the result of the OpenCV function
  printf("\t Info: Area and Contour Length \n");
  for( int i = 0; i< contours.size(); i++ )
	{
		
		area =  contourArea(contours[i]);
		p = arcLength( contours[i], true );

		//printf(" * Contour[%d] - Area (M_00) = %.2f - Area OpenCV: %.2f - Length: %.2f \n", i, mu[i].m00, contourArea(contours[i]), arcLength( contours[i], true ) );
	}

}