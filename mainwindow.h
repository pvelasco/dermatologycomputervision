#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
     void open();
	 void drawQTImage(QImage);
     void blur_image();
     void bt_reset_onclick();
     void bt_apply_click();
	 void bt_show_click();
     void boxFilter_image();
     void bilateralFilter_image();
     void GaussianBlur_image();
     void medianBlur_image();
     void ColorHistogram();
	 void laplaceFilter();
	 void sobelFilter();
	 void cannyFilter();	 
	 void CannyThreshold(int);
	 void convert_to_RGB();
	 void findContoursClick();
	 void ABCD_method();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
